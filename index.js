//console.log("Hi");

//Arrays
let students = [
	"Tony",
	"Peter",
	"Wanda",
	"Vision",
	"Loki"
];

console.log(students);

/*students.push("Thor");
console.log(students);

students.unshift("Steve");
console.log(students);

students.pop();
console.log(students);

students.shift();
console.log(students);

let arrNum = [15, 20, 25, 30, 11];

let allDivisible;
arrNum.forEach(num => {
	if(num % 5 === 0){
		console.log(`${num} is divisible by 5`);
	}else{
		allDivisible = false;
	}
});
console.log(allDivisible);

arrNum.pop();
arrNum.push(35);
console.log(arrNum);

let divisibleBy5 = arrNum.every(num => {
	console.log(num);
	return (num % 5 ===0);
});
console.log(divisibleBy5);

//Math
console.log(Math);
//Pre-defined properties
console.log(Math.E);	//Euler's number
console.log(Math.PI);	//Pi
console.log(Math.SQRT2);	//Square root of 2
console.log(Math.SQRT1_2);	//Square root of 1/2
console.log(Math.LN2);	//Natural logarithm of 2
console.log(Math.LN10);	//Natural logarithm of 10
console.log(Math.LOG2E);	//Base 2 logarithm of E
console.log(Math.LOG10E);	//Base 10 logarithm of E

//Math methods
console.log(Math.round(Math.PI));
console.log(Math.ceil(Math.PI));
console.log(Math.floor(Math.PI));
console.log(Math.trunc(Math.PI));
console.log(Math.sqrt(3.14));
console.log(Math.min(-4, -3, -2, -1, 0, 1, 2, 3, 4));
console.log(Math.max(-4, -3, -2, -1, 0, 1, 2, 3, 4));*/


//Function Coding - Activity

//Activity 1
function addToEnd(arr, str){
	if(typeof str == "string"){
		arr.push(str);
		return arr;
	}else{
		console.log("error - can only add strings to an array");
	}
};

//Activity 2
function addToStart(arr, str){
	if(typeof str == "string"){
		arr.unshift(str);
		return arr;
	}else{
		console.log("error - can only add strings to an array");
	}
};

//Activity 3
function elementChecker(arr, str){
	if(arr.length != 0){
		if(typeof str == "string"){
			let indexStr = arr.indexOf(str);
			if(indexStr > -1){
				return true;
			}else{
				return false;
			}
		}else{
			console.log("error - can only add strings to an array");
		}
	}else{
		console.log("error - passed in array is empty");
	}
};

//Activity 4
function checkAllStringEnding(arr, char){
	if(arr.length != 0){
		let isString = arr.every(item => {
			return typeof item == "string";
		});
		if(isString){
			if(typeof char == "string"){
				if(char.length == 1){
					let isLastLetterTheSame = arr.every(name => name.charAt(name.length-1) === char);
					return isLastLetterTheSame;
				}else{
					console.log("error - 2nd argument must be a single character");
				}
			}else{
				console.log("error - 2nd argument must be of data type string");
			}
		}else{
			console.log("error - all array elements must be strings");
		}
	}else{
		console.log("error - array must NOT be empty");
	}
};

//Activity 5
function stringLengthSorter(arr){
	let isString = arr.every(name => typeof name == "string");
	if(isString){
		return arr.sort();
	}else{
		console.log("error - all array elements must be strings");
	}
};

